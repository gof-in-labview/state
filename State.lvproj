﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="States" Type="Folder">
			<Item Name="ATM States.lvclass" Type="LVClass" URL="../ATM States/Interface/ATM States.lvclass"/>
			<Item Name="Has Card.lvclass" Type="LVClass" URL="../ATM States/Has Card/Has Card.lvclass"/>
			<Item Name="Has PIN.lvclass" Type="LVClass" URL="../ATM States/Has PIN/Has PIN.lvclass"/>
			<Item Name="No Card.lvclass" Type="LVClass" URL="../ATM States/No Card/No Card.lvclass"/>
			<Item Name="No Cash.lvclass" Type="LVClass" URL="../ATM States/No Cash/No Cash.lvclass"/>
		</Item>
		<Item Name="ATM.lvclass" Type="LVClass" URL="../ATM Class/ATM.lvclass"/>
		<Item Name="Main.vi" Type="VI" URL="../Main.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
